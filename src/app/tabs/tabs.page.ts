import { Component } from '@angular/core';
import { DbService } from '../db.service';
import { Players } from '../../players';
import { Ptasks } from 'src/ptasks';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  upplayers: Players [] = [];
  upallptasks: Ptasks [] = [];

  constructor(private db: DbService) {}

  /*Gets objects from storage when switching from other tabs to Tasks tab (tab2) so no restart/refresh is needed
  to display values if changes has been made*/
  update(){
      this.db.getPlayers()
      .then(data => {
        this.upplayers = JSON.parse(String(data));
        if (!this.upplayers) {
          this.upplayers = [];
        }
      });
      this.db.getTasks()
      .then(data => {
        this.upallptasks = JSON.parse(String(data));
        if (!this.upallptasks) {
          this.upallptasks = [];
        }
      });
    }
}
