import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Players } from '../players';
import { Ptasks } from '../ptasks';


@Injectable({
  providedIn: 'root'
})
export class DbService {

  constructor(private storage: Storage) { }

  //gets players from storage
  getPlayers() {
    return new Promise ((resolve) =>{
      this.storage.get('players').then(data =>{
        resolve(data);
      });
    });
  }

  //gets player made tasks from storage
  getTasks() {
    return new Promise ((resolve) =>{
      this.storage.get('ptasks').then(data =>{
        resolve(data);
      })
    });
  }

  //saves players to the storage
  savePlayers(players: Players [] = []){
    this.storage.set('players', JSON.stringify(players));
  }

  //saves player made tasks to storage
  saveTasks(ptasks: Ptasks [] = []){
    this.storage.set('ptasks', JSON.stringify(ptasks));
  }
}
