import { Component, OnInit } from '@angular/core';
import { DbService } from '../db.service';
import { Players } from '../../players';
import { DevTasks } from '../../devtasks';
import { Ptasks } from 'src/ptasks';
import { TabsPage } from '../tabs/tabs.page';
//Text to speech works only on mobile (tested it and it works)
import { TextToSpeech } from '@ionic-native/text-to-speech/ngx';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit {s

  players: Players [] = [];
  randomP: string = "Name of the player who gets the task appears here";
  alldevtasks: [] = [];
  bothtasks: string;
  allptasks: Ptasks [] = [];
  defswitch: boolean = true;
  pswitch: boolean = true;
  ttsswitch: boolean = false;
  donoff: string = "ON";
  ponoff: string = "ON";
  ttsonoff: string = "OFF";
  
  constructor(
    private db: DbService, 
    private update: TabsPage,
    private tts: TextToSpeech) {}

  //Loads players and player tasks from storage and devtasks from the correct folder when the app is launched.
  ngOnInit(){this.db.getPlayers()
    .then(data => {
      this.players = JSON.parse(String(data));
      if (!this.players) {
        this.players = [];
      }
    });
    this.db.getTasks()
    .then(data => {
      this.allptasks = JSON.parse(String(data));
      if (!this.allptasks) {
        this.allptasks = [];
      }
    });
    fetch('../../assets/devtasks/tasks.json').then(res => res.json())
    .then(json => {
      this.alldevtasks = json;
    });
    //Without this update method, first roll (if page is refreshed) will not display correct values
    this.update.update();
  }

  //Switch dev tasks on/off
  defOnOff(){
    if(this.defswitch === true){
      this.defswitch = false;
      this.donoff = "OFF";
    } else {
      this.defswitch = true;
      this.donoff = "ON";
    }
  }

  //Switch player tasks on/off
  pOnOff(){
    if(this.pswitch === true){
      this.pswitch = false;
      this.ponoff = "OFF"; 
    } else {
      this.pswitch = true;
      this.ponoff = "ON";
    }
  }

  //Switch text to speech on/off
  ttsOnOff(){
    if(this.ttsswitch === true){
      this.ttsswitch = false;
      this.ttsonoff = "OFF"; 
    } else {
      this.ttsswitch = true;
      this.ttsonoff = "ON";
    }
  }

  getRandoms(){
    /*placed another update method here so that you get up to date values even if you use back/forward buttons in browser
    (it takes one roll to get the updated values)*/
    this.update.update();
    //Gets updated players and tasks objects if changes have been made
    this.players = this.update.upplayers;
    this.allptasks = this.update.upallptasks;
    const devOrP = Math.floor(Math.random() * (this.alldevtasks.length + this.allptasks.length));
    const randomPlayer: Players = this.players[Math.floor(Math.random() * this.players.length)];
    //Checks if there are any players if not displays and reads out "add players first" message
    if(this.players.length === 0){
      this.randomP = "Add players first!";
      this.bothtasks = "";
      if(this.ttsswitch === true){
        this.tts.speak({
          text: this.randomP,
          locale: 'en-GB',
          rate: 0.85
        }); 
      }
    }
    //Adds "again" if same player gets two tasks in row to make it clear that button was pressed
    if(randomPlayer.name === this.randomP){
      this.randomP = randomPlayer.name + " again";
    } else{
      this.randomP = randomPlayer.name;
    }
    /*Checks if either or both of the tasks are switched off and if there is any player tasks made,
    then gets random dev or player task (if both are selected, scales based of the amount of player tasks)*/
    if(this.defswitch === false && this.pswitch === true){
      if(this.allptasks.length === 0){
        this.randomP = "No player tasks made"
        this.bothtasks = "";
      } else {
        const randomPtask: Ptasks = this.allptasks[Math.floor(Math.random() * this.allptasks.length)];
        this.bothtasks = randomPtask.ptasks;
      }
    } else if (this.defswitch === true && this.pswitch === false){
      const randomDevTask: DevTasks = this.alldevtasks[Math.floor(Math.random() * this.alldevtasks.length)];
      this.bothtasks = randomDevTask.devtasks;
    } else if (this.defswitch === true && this.pswitch === true){
      if(this.allptasks.length === 0){
        const randomDevTask: DevTasks = this.alldevtasks[Math.floor(Math.random() * this.alldevtasks.length)];
        this.bothtasks = randomDevTask.devtasks;
      } else if (devOrP < this.allptasks.length){
        const randomPtask: Ptasks = this.allptasks[Math.floor(Math.random() * this.allptasks.length)];
        this.bothtasks = randomPtask.ptasks;
      } else {
        const randomDevTask: DevTasks = this.alldevtasks[Math.floor(Math.random() * this.alldevtasks.length)];
        this.bothtasks = randomDevTask.devtasks;
      }
    } else {
      this.randomP = "Select tasks"
      this.bothtasks = "";
    }
    //Reads out the selected player name and task
    if(this.ttsswitch === true){
      this.tts.speak({
        text: this.randomP + "" + this.bothtasks,
        locale: 'en-GB',
        rate: 0.80
      });
    }
  }
}
