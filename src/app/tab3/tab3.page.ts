import { Component, OnInit } from '@angular/core';
import { DbService } from '../db.service';
import { Ptasks } from '../../ptasks';
import { AlertController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {

  tasks: Ptasks [] = [];
  playertasks: string = "";
  checkindex: number;
  edit: boolean = false;

  constructor(
    private db: DbService, 
    private alertController: AlertController,
    public toast: ToastController) {}
  
  /*Loads player made tasks from storage when the app is launched.*/
  ngOnInit(){
    this.db.getTasks()
    .then(data => {
      this.tasks = JSON.parse(String(data));
      if (!this.tasks) {
        this.tasks = [];
      }
    });
  }

  /*If the input is less than 3 characters long displays error message,
  if input is 3 or more characters input is valid and new task is saved to storage
  if edit is false player is creating a new task, if its true player is creating
  a new task and deleting the task selected for editing*/
  async addTask(){
    if(this.playertasks.length <= 2 && this.edit === false){
      const invalid = await this.toast.create({
        message: 'Task has to be atleast 3 characters long!',
        duration: 2000,
        position: "top",
        color: "secondary",
        cssClass: "center"
      });
      invalid.present();
    } else if (this.playertasks.length >= 3 && this.edit === false){
      const newTask: Ptasks = new Ptasks();
      newTask.ptasks = this.playertasks;
      this.tasks.push(newTask);
      this.db.saveTasks(this.tasks);
      this.playertasks = "";
    }else if(this.playertasks.length <= 2 && this.edit === true){
      const invalid = await this.toast.create({
        message: 'Task has to be atleast 3 characters long!',
        duration: 2000,
        position: "top",
        color: "secondary",
        cssClass: "center"
      });
      invalid.present();
    }else if(this.playertasks.length >= 3 && this.edit === true){
      const newTask: Ptasks = new Ptasks();
      newTask.ptasks = this.playertasks;
      this.tasks.push(newTask);
      this.db.saveTasks(this.tasks);
      this.playertasks = "";
      //uses the index gotten from editTask method to delete correct task
      const index = this.checkindex;
      this.tasks.splice(index, 1);
      this.db.saveTasks(this.tasks);
      this.edit = false;
    }
  }
  
  /*Displays confirmation alert and if chosen yes removes a task from the specific 
  ptasks object by using index as parameter*/
  async deleteTask(index) {
    const alert = await this.alertController.create({
      header: 'Delete task',
      message: 'Are you sure you want to <strong>delete</strong> this task?',
      buttons: [
        {
          text: 'No, cancel',
          role: 'cancel',
          cssClass: 'cancel',
      }, {
          text: 'Yes, delete',
          cssClass: 'yes',
          handler: () => {
            this.tasks.splice(index, 1);
            this.db.saveTasks(this.tasks);
          }
      }
    ]
    });
    await alert.present();
  }

  /*Gets correct task to the input field by using index as parameter and
   passes true to edit and index to checkindex to be used at addTask method*/
  editTask(index){
    this.playertasks = this.tasks[index].ptasks
    this.edit = true;
    this.checkindex = index;
    console.log(index)
  }
}
