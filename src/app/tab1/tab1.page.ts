import { Component, OnInit } from '@angular/core';
import { DbService } from '../db.service';
import { Players } from '../../players';
import { ToastController } from '@ionic/angular';
//Text to speech works only on mobile (tested it and it works)
import { TextToSpeech } from '@ionic-native/text-to-speech/ngx';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {

  players: Players [] = [];
  newPlayers: string = "";
  newSips: number;
  open: boolean;

  constructor(
    private db: DbService, 
    public toast: ToastController,
    private tts: TextToSpeech) {}

  //Loads players and sips from storage when the app is launched.
  ngOnInit(){
    this.db.getPlayers()
    .then(data => {
      this.players = JSON.parse(String(data));
      if (!this.players) {
        this.players = [];
      }
    });
  }

  /*If the input is less than 2 characters long displays error message when pressed enter,
  if input is 2 or more characters input is valid and new player is saved to storage*/
  async addPlayer(event){
    if(this.newPlayers.length <= 1 && event.keyCode === 13){
      const invalid = await this.toast.create({
        message: 'Name has to be atleast 2 characters long!',
        duration: 2000,
        position: "top",
        color: "secondary",
        cssClass: "center"
      });
      invalid.present();
    } else if (this.newPlayers.length >= 2 && event.keyCode === 13){
      const newPlayer: Players = new Players();
      newPlayer.name = this.newPlayers;
      this.players.push(newPlayer);
      this.db.savePlayers(this.players);
      this.newPlayers = "";
    }
  }

  /*Prevents people from accidentally adding "nothing" to sips resulting 
  in NaN being added to the players object. 
  By changing the newSips value to 0 only once player clicks the input field, 
  the placeholder text "Enter sips" is shown instead of the 0*/
  sipsOpen(){
    this.newSips = 0;
    this.open= true;
  }

  /*Removes sips from the specific players object by using index as parameter.
  Displays and reads out a *player name* is the winner message when players sips reach 0 or below*/
  async removeSips(index){
      const removeSip: Players = this.players[index];
      removeSip.sips = removeSip.sips - this.newSips;
      this.db.savePlayers(this.players);
      if(this.players[index].sips <= 0){
        const winner = await this.toast.create({
          message: this.players[index].name + " is the winner! Everyone else finish your remaining sips!",
          duration: 5000,
          position: "middle",
          color: "tertiary",
          cssClass: "small"
        });
        winner.present();
        //Did not include this in tts toggle, makes it funnier and doesnt get too obnoxious
        this.tts.speak({
          text: this.players[index].name + " is the winner! Everyone else finish your remaining sips!",
          locale: 'en-GB',
          rate: 0.85
        });
      }
  }

  //Adds sips to the specific players object by using index as parameter
  addSips(index){
    const addSip: Players = this.players[index];
    addSip.sips = addSip.sips + this.newSips;
    this.db.savePlayers(this.players);
  }

  //Removes a player from the specific players object by using index as parameter
  deletePlayer(index) {
    this.players.splice(index, 1);
    this.db.savePlayers(this.players);
  }
}